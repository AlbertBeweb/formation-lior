<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;

class ApplicationType extends AbstractType {
    /**
     * Permet de creer un template pour le form builder
     * 
     * @param string $label
     * @param string $placeholder
     * @param array $options
     * @return array
     */
    protected function getConfiguration($label, $placeholder, $options = [])
    {
        return array_merge_recursive([
                'label' => $label,
                'attr' => [
                    'placeholder' => $placeholder
                ]
        ], $options);
    }
}